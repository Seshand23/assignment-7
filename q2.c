#include<stdio.h>
#include<string.h>

int main()
{
    
    int count = 0;
    char word[25] = "Hello my name is Seshan";
    char letter = 's';
      
    for(int i = 0 ; word[i] != '\0' ; i++)
    {
        if(letter == word[i])
        {
            count++;
        }
        else 
        {
            continue;
        }
    }
    
    printf("Frequency of %c = %d \n", letter, count);
    
    return 0;
}
