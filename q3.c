#include<stdio.h>

int main()
{
	int a[2][2] = {{2,2},{3,3}}, b[2][2] = {{3,3},{4,4}}, add[2][2] = {0}, mult[2][2]={0};
	
	//MATRIX A
	printf("Matrix A : \n");
	for(int i = 0 ; i < 2 ; i++)
	{
		for(int j = 0 ; j < 2 ; j++)
		{
			printf("%d	", a[i][j]);
		}
		printf("\n");
	}
	
	printf("\n");

	//MATRIX B
	printf("Matrix B : \n");
        for(int i = 0 ; i < 2 ; i++)
        {
                for(int j = 0 ; j < 2 ; j++)
                {
                        printf("%d      ", b[i][j]);
                }
                printf("\n");
        }

	printf("\n");

	//ADDITION OF MATRICES
	printf("Addition of the Matrices A and B : \n");
	for(int i = 0 ; i < 2 ; i++)
	{
		for(int j = 0 ; j < 2 ; j++)
		{
			add[i][j] = a[i][j] + b[i][j];
		}
	}

	for(int i = 0 ; i < 2 ; i++)
	{
		for(int j = 0 ; j < 2 ; j++)
		{
			printf("%d	", add[i][j]);
		}
		printf("\n");
	}

	printf("\n");

	//MULTIPLICATION OF MATRICES
	printf("Multiplication of the Matrices A and B : \n");
	for(int i = 0 ; i < 2 ; i++)
	{
		for(int j = 0 ; j < 2 ; j++)
		{
			for(int k = 0 ; k < 2 ; k++)
			{
				mult[i][j] = mult[i][j] + (a[i][k] * b[k][j]);
			}
		}
	}	

	for(int i = 0 ; i < 2 ; i++)
	{
		for(int j = 0 ; j < 2 ; j++)
		{
			printf("%d	", mult[i][j]);
		}
		printf("\n");
	}

	return 0;	
}
